<?php

namespace Netmon\Interfaces\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use ApiServer\Authorization\Policies\BasePolicy;
use Netmon\Interfaces\Models\NetworkInterface;
use ApiServer\Users\Models\User;

class NetworkInterfacePolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function index(User $authUser) {
        //everyone is allowed to list interfaces
        return true;
    }

    public function store(User $authUser, NetworkInterface $interface = null) {
        //if authenticated user is allowed to update the coresponding
        //device, then he is also allowed to store a new interface
        try {
            if(!is_null($interface)) {
                if(\Gate::allows('update', $interface->device)) {
                    return true;
                }
            }
        } catch(\Exception $e) {}

        return $this->checkPermissions($authUser, 'store', 'interface');
    }

    public function show(User $authUser, NetworkInterface $interface) {
        //everyone is allowed to show interface
        return true;
    }

    public function update(User $authUser, NetworkInterface $interface) {
        if(\Gate::allows('update', $interface->device)) {
            return true;
        }

        return $this->checkPermissions($authUser, 'update', 'interface', $interface);
    }

    public function destroy(User $authUser, NetworkInterface $interface) {
        if(\Gate::allows('destroy', $interface->device)) {
            return true;
        }

        return $this->checkPermissions($authUser, 'destroy', 'interface', $interface);
    }
}
