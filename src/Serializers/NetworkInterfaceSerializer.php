<?php

namespace Netmon\Interfaces\Serializers;

use Gate;

use ApiServer\JsonApi\Serializers\BaseSerializer;

use Netmon\Interfaces\Models\NetworkInterface;

class NetworkInterfaceSerializer extends BaseSerializer
{
    protected $type = 'interfaces';

    public function getAttributes($model, array $fields = null)
    {
        if (! ($model instanceof NetworkInterface)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.NetworkInterface::class
            );
        }

        // set up attributes
        $attributes = $model->getAttributes();
        $attributes['created_at']  = $this->formatDate($model->created_at);
        $attributes['updated_at'] = $this->formatDate($model->updated_at);

        return $attributes;
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/interfaces/{$model->id}",
        ];

        //links to include based permissions
        if(Gate::allows('show', $model))
            $links['read'] = config('app.url')."/interfaces/{$model->id}";
        if(Gate::allows('update', $model))
            $links['update'] = config('app.url')."/interfaces/{$model->id}";
        if(Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/interfaces/{$model->id}";

        return $links;
    }

    /**
     * @return \Tobscure\JsonApi\Relationship
     */
    protected function device($model)
    {
        return $this->belongsTo($model, \Netmon\Devices\Serializers\DeviceSerializer::class);
    }
}

?>
