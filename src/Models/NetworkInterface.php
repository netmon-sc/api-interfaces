<?php

namespace Netmon\Interfaces\Models;

use ApiServer\Base\Models\BaseModel;
use ApiServer\Base\Traits\UuidForKeyTrait;

class NetworkInterface extends BaseModel
{
    use UuidForKeyTrait;

    /**
     * Bootstrap any application services.
     */
    public static function boot()
    {
        parent::boot();

        //Register validation service
        //on saving event
        self::saving(
            function ($model) {
                return $model->validate();
            }
        );
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'network_interfaces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // meta
		'device_id',

        // general
		'type',
        'name',
        'mac',
        'mtu',
        'txqueuelen',
        'autostart',
        'disabled',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Define default values of model. Example:
     * @var array
     */
    protected $attributes = [
        'type' => 'ethernet',
    ];

    /**
     * Holds the validation errors if some
     * @var unknown
     */
    protected $validationErrors = false;

    /**
     * Holds the validation rules
     * @var unknown
     */
    public $validationRules = [
        // These attributes are inspired by the NetworkInterface object of
        // the NetJSON specification http://netjson.org/rfc.html#rfc.section.5.4

        // meta
        'device_id' => 'required|exists:devices,id',

        // general
        'name' => 'required|string|between:1,15|unique_with:network_interfaces,device_id,ignore:{id}',
		'type' => 'required|in:ethernet,wireless,bridge,virtual,loopback,other',
        'mac' => 'nullable|string',
        'mtu' => 'nullable|integer',
        'txqueuelen' => 'nullable|integer',
        'autostart' => 'nullable|boolean',
        'disabled' => 'nullable|boolean',
    ];

    /**
     * n:1 relation to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device() {
    	return $this->belongsTo(\Netmon\Devices\Models\Device::class);
    }
}
