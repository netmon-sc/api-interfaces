<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkInterfaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_interfaces', function (Blueprint $table) {
        	//meta
            $table->uuid('id')->primary();
            $table->uuid('device_id');
            $table->foreign('device_id')
	            ->references('id')
    	        ->on('devices')
        	    ->onDelete('cascade');

            //general
            $table->string('name', 15)->nullable();
            $table->string('type', 20)->nullable();
            $table->macAddress('mac')->nullable();
            $table->integer('mtu')->nullable();
            $table->integer('txqueuelen')->nullable();
            $table->boolean('autostart')->nullable();
            $table->boolean('disabled')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('network_interfaces');
    }
}
