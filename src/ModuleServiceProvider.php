<?php

namespace Netmon\Interfaces;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        //register all the service provider this module needs
        $this->app->register(\Netmon\Interfaces\Providers\AuthServiceProvider::class);
        $this->app->register(\Netmon\Interfaces\Providers\EventServiceProvider::class);
        $this->app->register(\Netmon\Interfaces\Providers\RelationsServiceProvider::class);
    }

    public function boot()
    {
        require __DIR__ . '/Http/routes.php';

        //register database migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}

?>
