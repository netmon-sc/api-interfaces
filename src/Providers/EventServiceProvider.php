<?php

namespace Netmon\Interfaces\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use ApiServer\Authorization\Models\Permission;
use Netmon\Interfaces\Models\NetworkInterface;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        \Event::listen('api.interface.created', function (NetworkInterface $interface) {
            $user = \Auth::user();
            Permission::create([
                'user_id' => $user->id,
                'action_id' => 'show',
                'resource_id' => 'interface',
                'object_key' => $interface->id
            ]);
            Permission::create([
                'user_id' => $user->id,
                'action_id' => 'update',
                'resource_id' => 'interface',
                'object_key' => $interface->id
            ]);
            Permission::create([
                'user_id' => $user->id,
                'action_id' => 'destroy',
                'resource_id' => 'interface',
                'object_key' => $interface->id
            ]);
        });
    }
}
