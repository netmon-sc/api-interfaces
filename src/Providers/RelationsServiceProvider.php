<?php

namespace Netmon\Interfaces\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class RelationsServiceProvider extends ServiceProvider
{
    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        add_model_relation(
            'Netmon\Devices\Models\Device',
            'networkInterfaces',
            function($model) {
                return $model->hasMany('Netmon\Interfaces\Models\NetworkInterface');
            }
        );

        add_serializer_relation(
            'Netmon\Devices\Serializers\DeviceSerializer',
            'networkInterfaces',
            function($serializer, $model) {
                return $serializer->hasMany($model, 'Netmon\Interfaces\Serializers\NetworkInterfaceSerializer');
            }
        );
    }
}
