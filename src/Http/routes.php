<?php

Route::group([
    'namespace' => '\Netmon\Interfaces\Http\Controllers',
    'middleware' => 'cors'], function()
{
	/**
	 * Routes using JWT auth
	 */
	Route::group(['middleware' => ['auth.jwt']], function () {
        Route::resource('network-interfaces', NetworkInterfacesController::class, ['only' => [
			    'index', 'show', 'store', 'update', 'destroy'
		]]);
	});
});

?>
