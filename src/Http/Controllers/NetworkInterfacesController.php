<?php

namespace Netmon\Interfaces\Http\Controllers;

use ApiServer\JsonApi\Http\Controllers\DefaultResourceController;

class NetworkInterfacesController extends DefaultResourceController
{
    public function model() {
      return \Netmon\Interfaces\Models\NetworkInterface::class;
    }

    public function serializer() {
        return \Netmon\Interfaces\Serializers\NetworkInterfaceSerializer::class;
    }

    public function resource() {
        return "network-interfaces";
    }
}
