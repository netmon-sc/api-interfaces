# Creating a release
 1. Use [semantic versioning](http://semver.org/) for release versions
 1. Adjust 3rd party depencies in [composer.json](composer.json)
 1. Update 3rd party depencies using `composer update`
 1. Commit composer update using `git commit -sam`
 1. Create a signed tag and add a release description to the tag using `git tag -a -s <VERSION> <COMMIT-ID>`
 1. Push tags using `git push --tags`
