## Unittesting
From within a working Netmon server installation run:
```
cd vendor/netmon-server/interfaces
phpunit --bootstrap ../../../bootstrap/autoload.php
```
