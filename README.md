# Interfaces Module for the Netmon API-Server
This project holds the Interfaces module for the Netmon API-Server. It depends on the Devices module and allows to add multiple network interfaces to a device so that you can get regular status updates about traffic and connections on an interface.

## Installation
This module is automatically downloaded on the installation of the Netmon server
so you dont need to require it manually using `composer require`.

Just install and configure the module from within the root folder of
your Netmon Server installation by executing the following commands:
```
php artisan module:install Interfaces "Netmon\Interfaces\ModuleServiceProvider"
php artisan module:configure Interfaces
```

## Documentation
 * [Technology stack](doc/technology.md)
 * [Unittesting](doc/unittesting.md)
 * [Creating a release](doc/release.md)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

## Mailinglist
https://lists.ffnw.de/mailman/listinfo/netmon-dev

# License
See [License](LICENSE.txt)
