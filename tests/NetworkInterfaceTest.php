<?php

namespace Netmon\Interfaces\Tests;

use Netmon\Server\Tests\ModuleTestCase;
use Netmon\Server\Tests\UserTest;
use Netmon\Devices\Tests\DeviceTest;
use Netmon\Interfaces\Models\NetworkInterface;

class NetworkInterfaceTest extends ModuleTestCase
{
    protected function moduleServiceProvider() {
        return \Netmon\Interfaces\ModuleServiceProvider::class;
    }

    protected function moduleServiceProviderDepencies() {
        return [
            \Netmon\Devices\ModuleServiceProvider::class,
        ];
    }

    public function getStructure() {
		return [
			'type',
			'id',
			'attributes' => [
				'name',
                'type',
			]
		];
	}

    public static function createNetworkInterface($attributes = []) {
		$networkInterfaceCount = (NetworkInterface::get()->count())+1;

        if(!isset($attributes['name']))
            $attributes['name'] = "iface{$networkInterfaceCount}";

	    return NetworkInterface::create($attributes);
	}

	public static function createNetworkInterfaces($howMany) {
		$networkInterfaces = [];
		for($i = 0; $i < $howMany; $i++) {
			$networkInterfaces[] = static::createNetworkInterface();
		}
		return $networkInterfaces;
	}


    public function testDeviceCreatorCanCreateInterface() {
		$user = UserTest::createUser();
		$device = DeviceTest::createDevice([
            'creator_id' => $user->id
        ]);

        $authToken = \JWTAuth::fromUser($user);
		$headers = ['Authorization' => "Bearer {$authToken}"];

		$this->json(
			'POST',
			'/network-interfaces',
			[
				'data' => [
					'type' => "network-interface",
					'attributes' => [
                        'device_id' => $device->id,
						'name' => "wlan0",
						'type' => "wireless",
					]
				]
			],
			$headers
		);
		$this->assertResponseStatus(201);
		$this->seeJsonStructure($this->getResourceStrucure());
    }
}
?>
